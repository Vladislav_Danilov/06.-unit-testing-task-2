package test;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import calculatorTwo.CalculateException;

@RunWith(Parameterized.class)
public class CalculatorTest {

  @Parameter
  public Double numberOne;
  @Parameter(value = 1)
  public Double numberTwo;
  @Parameter(value = 2)
  public Double numberThree;

  @Parameters
  public static Collection<Object[]> data() {
    Object[][] data = new Object[][] {{1.0, 2.0, 3.0}, {5.0, 3.0, 3.0}, {5.0, 3.0, 3.0}};
    return Arrays.asList(data);
  }

  @Test
  public void testAddition() throws CalculateException {
    assertEquals("Addition result" + (numberOne + numberTwo + numberThree),
        Double.valueOf(numberOne + numberTwo + numberThree),
        Utils.calculator(new StringMathCreator().addition(numberOne, numberTwo, numberThree)));
  }

  @Test
  public void testMultiply() throws CalculateException {
    assertEquals("Multiply result" + (numberOne * numberTwo * numberThree),
        Double.valueOf(numberOne * numberTwo * numberThree),
        Utils.calculator(new StringMathCreator().multiply(numberOne, numberTwo, numberThree)));
  }

  @Test(expected = CalculateException.class)
  public void testDivisionException() throws CalculateException {
    Utils.calculator("2/0\r");
  }

  public void testDivision() throws CalculateException {
    assertEquals("Division result" + (numberOne / numberTwo / numberThree),
        Double.valueOf(numberOne / numberTwo / numberThree),
        Utils.calculator(new StringMathCreator().division(numberOne, numberTwo, numberThree)));
  }

  @Test(expected = CalculateException.class)
  public void testFewOperators() throws CalculateException {
    Utils.calculator("++2-3\r");
  }

  @Test(expected = CalculateException.class)
  public void testEmptyStrings() throws CalculateException {
    Utils.calculator("\r");
  }

  @Test(expected = CalculateException.class)
  public void testIllegalSimbol() throws CalculateException {
    Utils.calculator("2*2*a\r");
  }

  @Test
  public void testSubsctraction() throws CalculateException {
    assertEquals("Subsctraction result" + (numberOne - numberTwo - numberThree),
        Double.valueOf(numberOne - numberTwo - numberThree),
        Utils.calculator(new StringMathCreator().subsctraction(numberOne, numberTwo, numberThree)));
  }

  static class StringMathCreator {
    public String multiply(Double i, Double j, Double y) {
      return "(" + i + "*" + j + ")" + "*" + y + "\r";
    }

    public String division(Double i, Double j, Double y) {
      return i + "/" + j + "/" + y + "\r";
    }

    public String subsctraction(Double i, Double j, Double y) {
      return i + "-" + j + "-" + y + "\r";
    }

    public String addition(Double i, Double j, Double y) {
      return i + "+" + j + "+" + y + "\r";
    }
  }

}
