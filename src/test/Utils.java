package test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import calculatorTwo.CalculateException;
import calculatorTwo.Calculator;
import calculatorTwo.GeneratePolishNotation;

public class Utils {
  public static Double calculator(String stringMath) throws CalculateException {
    String stringTest = stringMath;
    InputStream inputStream =
        new ByteArrayInputStream(stringTest.getBytes(Charset.forName("UTF-8")));
    BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
    GeneratePolishNotation generatePolishNotation = new GeneratePolishNotation();
    Calculator calculator = new Calculator();
    Double result = null;
      try {
        result = calculator.calculate(generatePolishNotation.reversePolishNotation(bufferedReader));
      } catch (IOException e) {
        e.printStackTrace();
      }
    try {
      bufferedReader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }
}
