package calculatorTwo;

public class OperatorAnaliser {


  /**
   * Checks authorized operators
   * 
   * @param tokenOperator
   * @return true operator has, false no operator
   */
  public boolean operatorChecking(char tokenOperator) {
    switch (tokenOperator) {
      case TokenPak.MINUS:
      case TokenPak.PLUS:
      case TokenPak.MULTI:
      case TokenPak.DIVIDE:
        return true;

    }
    return false;
  }

  /**
   * Returns priority operation
   * 
   * @param operation char
   * @return byte
   */
  public byte priorityOperation(char operation) {
    switch (operation) {
      case TokenPak.MULTI:
      case TokenPak.DIVIDE:
        return 2;
    }
    return 1; // ��� �������� + � -
  }
}
