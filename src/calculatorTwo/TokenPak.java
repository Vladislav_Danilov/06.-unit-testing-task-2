package calculatorTwo;

public class TokenPak {
  public final static char PLUS = '+';
  public final static char MINUS = '-';
  public final static char DIVIDE = '/';
  public final static char MULTI = '*';
  public final static char RIGHTBRACKET = '(';
  public final static char LEFTBRACKET = ')';
}
