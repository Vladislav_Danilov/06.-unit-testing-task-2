package calculatorTwo;

/**
 * Exception characterizes the error of reading and calculation
 * 
 * @author Vladislav_Danilov
 *
 */
public class CalculateException extends Exception {

  private static final long serialVersionUID = 1L;

  public CalculateException() {}

  public CalculateException(String message, Throwable exception) {
    super(message, exception);
  }

  public CalculateException(String message) {
    super(message);
  }

  public CalculateException(Throwable exception) {
    super(exception);
  }
}
