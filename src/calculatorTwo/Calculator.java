package calculatorTwo;

import java.util.*;

/**
 * 
 * This class converts and finds expression on Polish Notation
 * 
 * @author Vladislav_Danilov
 *
 */
public class Calculator {

  OperatorAnaliser operatorAnaliser;



  /**
   * Counts of expression in Reverse Polish Notation
   * 
   * @param stringPolishNotation input string Polish Notation
   * @return double result express decision
   */
  public Double calculate(String stringPolishNotation) throws CalculateException {
    Double currentVariable = Double.valueOf(0);
    Double nextVariable = Double.valueOf(0);
    String tempString;
    operatorAnaliser = new OperatorAnaliser();
    Deque<Double> stack = new ArrayDeque<Double>();
    StringTokenizer st = new StringTokenizer(stringPolishNotation);
    while (st.hasMoreTokens()) {
      tempString = st.nextToken().trim();
      if (1 == tempString.length() && operatorAnaliser.operatorChecking(tempString.charAt(0))) {
        if ((nextVariable = stack.poll()) == null) {
          throw new CalculateException("More operator");
        }
        if ((currentVariable = stack.poll()) == null) {
          throw new CalculateException("More operator");
        }
        switch (tempString.charAt(0)) {
          case TokenPak.PLUS:
            currentVariable += nextVariable;
            break;
          case TokenPak.MINUS:
            currentVariable -= nextVariable;
            break;
          case TokenPak.DIVIDE:
            currentVariable /= nextVariable;
            break;
          case TokenPak.MULTI:
            currentVariable *= nextVariable;
            break;
          default:
            break;
        }
        stack.push(currentVariable);
      } else {
        try {
          currentVariable = Double.parseDouble(tempString);
          stack.push(currentVariable);
        } catch (NumberFormatException e) {
          e.printStackTrace();
          throw new CalculateException("Illegal element " + tempString);
        }
      }
    }
    if (stack.size() == 0) {
      throw new CalculateException("Empty string");
    } else if (stack.peek().equals(Double.POSITIVE_INFINITY)) {
      throw new CalculateException("Divide zero");
    }

    return stack.poll();
  }
}
