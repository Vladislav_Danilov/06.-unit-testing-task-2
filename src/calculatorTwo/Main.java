package calculatorTwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class Main {

  public static void main(String[] args) throws Exception {
    BufferedReader streamFromTheConsole =
        new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));
    Calculator calculator = new Calculator();
    GeneratePolishNotation generatePolishNotation = new GeneratePolishNotation();
    try {
      System.out.println("������ ��������� ��� �������:");
      System.out.println(
          calculator.calculate(generatePolishNotation.reversePolishNotation(streamFromTheConsole)));
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    try {
      streamFromTheConsole.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
