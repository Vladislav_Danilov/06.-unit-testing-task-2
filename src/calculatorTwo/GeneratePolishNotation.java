package calculatorTwo;

import java.io.BufferedReader;
import java.io.IOException;

public class GeneratePolishNotation {
  private char token;
  OperatorAnaliser operatorAnaliser;

  /**
   * Converts expression in Polish notation
   * 
   * @return polish notation string
   * @throws CalculateException
   * @throws IOException
   */
  public String reversePolishNotation(BufferedReader bufferedReader)
      throws CalculateException, IOException {
    StringBuilder concentratorTemp = new StringBuilder("");
    StringBuilder concentratorDigite = new StringBuilder("");
    operatorAnaliser = new OperatorAnaliser();
    char cTmp;

    while (token != '\r') {
      token = (char) bufferedReader.read();
      if (operatorAnaliser.operatorChecking(token)) {
        while (concentratorTemp.length() > 0) {
          cTmp = concentratorTemp.substring(concentratorTemp.length() - 1).charAt(0);
          if (operatorAnaliser.operatorChecking(cTmp) && (operatorAnaliser
              .priorityOperation(token) <= operatorAnaliser.priorityOperation(cTmp))) {
            concentratorDigite.append(" ").append(cTmp).append(" ");
            concentratorTemp.setLength(concentratorTemp.length() - 1);
          } else {
            concentratorDigite.append(" ");
            break;
          }
        }
        concentratorDigite.append(" ");
        concentratorTemp.append(token);
      } else if (TokenPak.RIGHTBRACKET == token) {
        concentratorTemp.append(token);
      } else if (TokenPak.LEFTBRACKET == token) {
        cTmp = concentratorTemp.substring(concentratorTemp.length() - 1).charAt(0);
        while (TokenPak.RIGHTBRACKET != cTmp) {
          if (concentratorTemp.length() < 1) {
            throw new CalculateException("Error brackets.");
          }
          concentratorDigite.append(" ").append(cTmp);
          concentratorTemp.setLength(concentratorTemp.length() - 1);
          cTmp = concentratorTemp.substring(concentratorTemp.length() - 1).charAt(0);
        }
        concentratorTemp.setLength(concentratorTemp.length() - 1);
      } else {
        // ���� ������ �� �������� - ��������� � �������� ������������������
        concentratorDigite.append(token);
      }
    }

    // ���� � ����� �������� ���������, ��������� �� � ������� ������
    while (concentratorTemp.length() > 0) {
      concentratorDigite.append(" ")
          .append(concentratorTemp.substring(concentratorTemp.length() - 1));
      concentratorTemp.setLength(concentratorTemp.length() - 1);
    }
    return concentratorDigite.toString();
  }
}
